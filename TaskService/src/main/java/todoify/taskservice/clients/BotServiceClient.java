package todoify.taskservice.clients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import todoify.taskservice.models.TodoifyReplyMessage;
import org.springframework.web.bind.annotation.*;


@FeignClient("botService")
public interface BotServiceClient {
    @PostMapping("/send")
    void send(@RequestBody TodoifyReplyMessage replyMessage);
}

