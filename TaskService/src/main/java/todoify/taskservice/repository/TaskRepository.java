package todoify.taskservice.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import todoify.taskservice.entity.TaskModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<TaskModel,String> {
    List<TaskModel> findAllByTitle(String title);

    Optional<TaskModel> findByTitle(String title);
}
